const fs = require('fs');
const axios = require('axios');
const commands = [];

const botConfig = JSON.parse(fs.readFileSync('bot.json','utf8'));

if (!String.prototype.format) {
  String.prototype.format = function() {
    var args = arguments;
    return this.replace(/{(\d+)}/g, function(match, number) { 
      return typeof args[number] != 'undefined'
        ? args[number]
        : match
      ;
    });
  };
}

const handleMessageOptions = (option, params, message) => {
  // remove the current option.
  params = params.splice(1)
  // Does the option require any paramenters
  if (option.params && !isNaN(option.params) && option.params > 0) {
    // Make sure enough have been supplied, otherwise, report error
    if (params.length === option.params) {
      // Okay, good, now handle these.
      message.channel.send(option.message.format(...params));
    } else {
      // Bad, show the error
      message.channel.send(option.error || 'Something went wrong with the previous command');
    }
  } else {
    // Is there still unparsed options?
    if (params.length > 0) {
      // Yes, handle them
      // Get the next option to deal with
      const nextOption = option.options
              .find(x => params[0] === x.option);
      
      handleMessageOptions(nextOption, params, message);
    } else {
      // Just send out this options message
      message.channel.send(option.message);
    }
  }
};

const parseSystemTags = (outputMessage, inputMessage) => {
  outputMessage = outputMessage.replace('{user}', inputMessage.author);
  return outputMessage;
};

const getParams = (command, message) => {
  return message
    .replace(command, '')
    .trim()
    .split(' ')
    .filter(x => x.length > 0);
};

const handleNormalMessage = (command, message) => {
  const commandName = botConfig.commandSymbol + command.command;
  const params = getParams(commandName, message.content);
  command.message = parseSystemTags(command.message, message);
  
  if (command.options && params.length > 0) {
    const option = command.options.find(x => params[0] === x.option);
    handleMessageOptions(option, params, message);
    return;
  }

  message.channel.send(command.message);
};

const handleFunctionMessage = (command, message) => {
  const commandName = botConfig.commandSymbol + command.command;
  const params = getParams(commandName, message.content);
  // Check function type
  switch (command.do.type) {
    case 'get':
      axios.get(command.do.content)
        .then(response => {
          message.channel.send(response.data);
        })
        .catch(error => {

        });
      break;
    case 'exec':
      const result = exec(command.do.content.format(...params));
      message.channel.send(command.do.message.format(result));
      break;
  }
};

const exec = (script) => {
  return Function('"use strict"; return (' + script + ')')();
}

botConfig.commands.forEach(command => {
  commands[command.command] = (message) => {
    let fn = null;
    if (command.message) {
      fn = handleNormalMessage;
    } else if (command.do) {
      fn = handleFunctionMessage;
    } else {
      // Unrecognised
    }

    if (fn) {
      fn(command, message);
    }
  };
});

commands['help'] = (message) => {
  let text = '';
  for(command in commands) {
    text += `${command}`;
  }
  message.channel.send(text);
};

commands['about'] = (message) => {
  let text = `Name: ${botConfig.about.name}
Author: ${botConfig.about.author}
Version: ${botConfig.about.version}
DIY Version: ${require('./../package.json').version}

${botConfig.about.description}`;
  message.channel.send(text);
};

module.exports = {
  handleCommand: (command, message) => {
    if (command && commands[command]) {
      commands[command](message);
    }
  }
}
