
const Discord = require('discord.js');
const env = require('dotenv');
const fs = require('fs');
const commands = require('./commands');

const client = new Discord.Client();

const botConfig = JSON.parse(fs.readFileSync('bot.json','utf8'));

env.config();

client.on('ready', () => {
  console.log(`Logged in as ${client.user.tag}`);
  if (botConfig.startup.channel && botConfig.startup.channel.length > 0) {
    client.channels
      .find(c => c.name === botConfig.startup.channel)
      .send(botConfig.startup.message);
  }
});

client.on('message', message => {
  // Ignore self and other bots
  if (message.author != client.user && !message.author.bot) {
    if (message.content.startsWith(botConfig.commandSymbol)) {
      const command = message.content.substr(botConfig.commandSymbol.length || 0).split(" ")[0].toLowerCase();
      commands.handleCommand(command, message);
    }
  }
});

client.login(process.env.BOT_TOKEN);
